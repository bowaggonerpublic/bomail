####
# bomail.util.addr
#
# Utilities for dealing with email addresses
# and the "address book", which is stored in config.addr_book_file.
####

import sys
import os
from bisect import bisect_left
import email.utils

from bomail.config.config import pathcfg,sendcfg
import bomail.cli.mailfile as mailfile
import bomail.util.merge_lines as merge_lines
import bomail.util.util as util

####
# Address book file format: each line is
#    email_addr, N M
# where N and M are integers for num emails sent and received respectively.
# i.e. the user has sent N emails to them and received M emails from them.
#
# Python 'types' used:
# pr      A pair of strings of the form (display name, local@domain)
# prlist  A list of the form [pr, pr, ...]
#
# Plain text stuff:
#
# email_addr  An email address string of the form
#             display name <local@domain>
#             or local@domain
#             (see notes on RFC 5322 below)
####

# Ideally: email addresses could be any RFC 5322 valid recipient
# Currently: groups are not supported

#RFC5322_specials = ["(" , ")" , 
#                    "<" , ">",
#                    "[" , "]",
#                    ":" , ";",
#                    "@" , "\\",
#                    "," , ".",
#                    '"']
#
#RFC5322_specials_set = set(RFC5322_specials)
#
#WHITESPACE = [' ', '\t', '\n', '\r']
#MYSKIP_CHARS = [' ', '\t', '\n', '\r', ',']

# This is just to guess when the user is the recipient of a message (err on the side of yes)
# canonical version that ignores case, dots, and +stuff in
# email address
# input is the second part of a pair (e.g. "local@domain")
def canon(s):
  if "@" not in s:
    return s  # no idea what's going on
  words = s.split("@")
  local, domain = words[0], words[1]
  local = local.lower()
  if "+" not in local and "." not in local:
    return s
  if "+" in local:
    local = local[:local.index("+")]
  if "." in local:
    local = local.replace(".","")
  return local + "@" + domain


# GLOBAL VARIABLE
# the user's list of email addresses
my_canon_addr_list = [canon(a) for a in sendcfg.my_aliases + sendcfg.email_addrs]





# given a line from address book,
# return email pair and N, M
def str_to_quad(s):
  if "," not in s:
    p = util.str_to_pair(s)
    return (p[0], p[1], 0, 0)
  l = s.split(",")
  p = util.str_to_pair(l[0].strip())
  try:
    counts = l[1].split()
    return (p[0], p[1], int(counts[0].strip()), int(counts[1].strip()))
  except:
    return (p[0], p[1], 0, 0)


# given email pair and two counts,
# return a line for the address book
def quad_to_str(q):
  return util.pair_to_str(q[0], q[1]) + " , " + str(q[2]) + " " + str(q[3])


# check if the emails go to the same place
# not used! deprecated
#def is_recip_eq(p1, p2):
#  return canon(p1) == canon(p2)

def is_pair_me(pr):
  return canon(pr[1]) in my_canon_addr_list

def is_str_me(s):
  return is_pair_me(util.str_to_pair(s))


# pair_to_count is a dictionary mapping (name, email) to (#sent, #recv)
def write_addr_file(pair_to_count):
  # add my entry_quads to the dict, then flatten it to a list and write to disk
  entry_quads = [(p[0], p[1], c[0], c[1]) for p,c in pair_to_count.items()]
  entry_quads.sort()
  s = "\n".join([quad_to_str(q) for q in entry_quads])
  with open(pathcfg.addr_book_file, "w") as f:
    f.write(s)



# Mostly, this class is just used to read the list of addresses
# and to write new ones into the text file
class AddrBook:
  def __init__(self):
    self.pair_to_count = {}    # map (name, email_addr) to (send_count, recv_count)
    self.address_to_pair = {}    # map email_addr to (name, email_addr)
    self.load()


  def load(self):
    if not os.path.exists(pathcfg.addr_book_file):
      with open(pathcfg.addr_book_file, "w") as f:
        pass
    else:
      with open(pathcfg.addr_book_file) as f:
        lines = f.readlines()
      for l in [ll.strip() for ll in lines if ll.strip() != ""]:
        q = str_to_quad(l)
        self.pair_to_count[(q[0], q[1])] = (q[2], q[3])
        self.address_to_pair[q[1]] = (q[0], q[1])


  def check_for_new(self, prlist):
    added = False
    for pr in prlist:
      tuple_pr = (pr[0], pr[1])
      if tuple_pr not in self.pair_to_count:
        self.pair_to_count[tuple_pr] = (0, 0)
        self.address_to_pair[pr[1]] = tuple_pr
        added = True
    if added:
      self.rewrite_file()


  # delete all addresses in pairlist
  def remove_addresses(self, pairlist):
    rmlist = [p for p in pairlist if p in self.pair_to_count]
    if len(rmlist) != 0:
      for p in rmlist:
        del self.pair_to_count[p]
        if p[1] in self.address_to_pair:
          del self.address_to_pair[p[1]]
      self.rewrite_file()


  # delete all addresses in pairlist and add their
  # send/receive counts to merge_to
  def merge_addresses(self, merge_to, pairlist):
    c1, c2 = self.pair_to_count[merge_to]
    for pr in pairlist:
      t1, t2 = self.pair_to_count[pr]
      c1 += t1
      c2 += t2
      del self.pair_to_count[pr]
      del self.address_to_pair[pr[1]]
    self.pair_to_count[merge_to] = (c1, c2)
    self.rewrite_file()


  # look up pr in the book
  # if not present, but pr only contains an address
  # that is present, then change to that address
  def lookup_and_change(self, pr):
    if pr in self.pair_to_count:
      return pr
    if pr[0] == "" and pr[1] in self.address_to_pair:
      return self.address_to_pair[pr[1]]
    return pr


  # update our data to include any new addresses and new
  # send/receive counts
  def update_for_new(self, maildatas):
    for data in maildatas:
      fromlist = util.str_to_pairlist(data[mailfile.FROM_L])
      tolist = util.str_to_pairlist(data[mailfile.TO_L]) + util.str_to_pairlist(data[mailfile.CC_L]) + util.str_to_pairlist(data[mailfile.BCC_L])
      fromlist = [self.lookup_and_change(pr) for pr in fromlist]
      tolist = [self.lookup_and_change(pr) for pr in tolist]
      # make default entries for everything we haven't seen
      for p in [pr for pr in fromlist+tolist if pr not in self.pair_to_count]:
        self.pair_to_count[p] = (0, 0)
        self.address_to_pair[p[1]] = p
      # increment counts depending on if sent or received
      if data[mailfile.SENT_L] == "True":
        for p in tolist:
          cnt = self.pair_to_count[p] if p in self.pair_to_count else (0, 0)
          self.pair_to_count[p] = (cnt[0] + 1, cnt[1])
      else:
        for p in fromlist:
          cnt = self.pair_to_count[p] if p in self.pair_to_count else (0, 0)
          self.pair_to_count[p] = (cnt[0], cnt[1] + 1)
    self.rewrite_file()

 
  # save our address book to the text file
  def rewrite_file(self):
    write_addr_file(self.pair_to_count)

