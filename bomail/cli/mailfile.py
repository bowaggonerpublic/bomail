####
# bomail.cli.mailfile
#
# Specifies the bomail file format, called 'mailfile',
# and mail manager object, and some commands to view/edit
# metadata of emails.
####

import sys
import os, bisect
import email.utils, uuid
import traceback
import datetime, dateutil.parser

from bomail.config.config import pathcfg
import bomail.util.tags as tags
import bomail.util.attach as attach
import bomail.util.msgids as msgids
import bomail.util.datestuff as datestuff
import bomail.util.util as util

####
# This file specifies
# (1) the mailfile format and interface
# (2) MailMgr, the mail manager object
#
# Emails are stored in "mailfiles".
# A mailfile is of the form:
#   field_name: field_value
#   ...
#   field_name: field_value
#   =================
#   body_of_email
#
####

fields = ["from","to","cc","bcc","reply-to","subject","date","sent","msg-id","references","referenced_by","attachments","state","tags","body"]
FROM_L   = 0    # addr list, as defined in bomail.util.addr
TO_L     = 1    # addr list
CC_L     = 2    # addr list
BCC_L    = 3    # addr list
REPLY_L  = 4    # addr list (this is the reply-to header)
SUBJ_L   = 5    # any string (no newlines allowed)
DATE_L   = 6    # ISO8061 format date (including timezone)
SENT_L   = 7    # "True" if sent from me, "False" otherwise
MSG_ID_L = 8    # email message id enclosed in < >
REFS_L   = 9    # comma-and-space-separated list of msg_ids
REF_BY_L = 10   # comma-and-space-separated list of msg_ids
ATTACH_L = 11   # comma-and-space-separated list of attachment paths, each quote-enclosed (inner quotes and backslashes escaped with backslashes)
STATE_L  = 12   # "open", "closed", or "scheduled datestr" where datestr is ISO8061 str in UTC
TAGS_L   = 13   # comma-and-space-separated list of tags (not quoted, no commas in them)
BODY_L   = 14   # arbitrary string

sep = "================\n"  # between headers and body

usage_str = """
Usage: reads a list of filenames from stdin and does the action for all of them.
Options: -g (get the indicated fields), -add-tags, -rm-tags

  mailfile.py -h                  # print this help
  mailfile.py -g all              # get and print all fields
  mailfile.py -g from bcc date    # get and print the fields 'from', 'bcc', and 'date'
  mailfile.py -add-tags tag1 tag2 # add the tags 'tag1' and 'tag2'
  mailfile.py -rm-tags tag1 tag2  # remove the tags 'tag1' and 'tag2'
"""


error_data = [
  "error file not found <>", "", "", "", "",  # from, to, cc, bcc, reply-to
  "", "9999-12-28", "False", email.utils.make_msgid(uuid.uuid4().hex), # subj, date, sent, msgid
  "", "", "", "open", "", # refs, ref-by, attach, state, tags
  "error file not found"]  # body



#-------------------------------
# fileid is of the form yyyy/mm-dd/THH:MM-msgid.ext

DRAFT_EXT = ".draft"
MAIL_EXT = ".mail"
MIME_EXT = ".mime"

MSGID_PATH_MAXLEN = 76

def msgid_as_path(msg_id):
  return msg_id.replace("/","-")[:MSGID_PATH_MAXLEN]

# assume datestr is in UTC
def get_datestart_file_part(datestr, msg_id):
  # convert date to utc
  obj = datestuff.parse_to_utc_obj(datestr)
  s = obj.strftime("%Y/%m-%d/T%H:%M:%S")    # maybe not cross-platform
  return s + "-" + msgid_as_path(msg_id)

def get_datestart_filename(datestr, msg_id, is_draft):
  s = get_datestart_file_part(datestr, msg_id)
  ext = DRAFT_EXT if is_draft else MAIL_EXT
  return s + ext

# return full path to mailfile
def get_filename(datestr, msg_id, is_draft):
  s = get_datestart_filename(datestr, msg_id, is_draft)
  d = pathcfg.drafts_dir if is_draft else pathcfg.email_dir
  return os.path.join(d, s)

# where its attachments are stored
def get_attach_dir(datestr, msg_id):
  s = get_datestart_file_part(datestr, msg_id)
  return os.path.join(pathcfg.attach_dir, s)

def get_trash_attach_dir(datestr, msg_id):
  s = get_datestart_file_part(datestr, msg_id)
  return os.path.join(pathcfg.trash_dir, "attach", s)

# return full file path
def get_trashfilename(datestr, msg_id, is_draft):
  s = get_datestart_filename(datestr, msg_id, is_draft)
  if is_draft:
    return os.path.join(pathcfg.trash_dir, "draft", s)
  return os.path.join(pathcfg.trash_dir, s)

# return fullfile path
def get_rawfilename(datestr, msg_id):
  s = get_datestart_file_part(datestr, msg_id)
  return os.path.join(pathcfg.old_rawmail_dir, s + MIME_EXT)


#--------------------------------------
# reading/writing to files

# given string,
# return data, which is just a list of the entries for each field
def parse_file_contents(contents, filename):
  data = []
  try:
    start_ind = 0
    for i in range(len(fields)-1):
      stop_ind = contents.index("\n", start_ind)
      if contents[start_ind:start_ind+len(fields[i])+1] != fields[i] + ":":
        raise Exception("Reading file: " + str(filename) + ", Field names don't match on line " + str(i+1))
      data.append(contents[start_ind+len(fields[i])+2 : stop_ind].strip())  # get actual data
      start_ind = stop_ind + 1
    start_ind = contents.index("\n", start_ind) + 1  # skip separator line =========
    data.append(contents[start_ind:])  # body
    return data
  except:
    util.err_log("mailfile: error parsing " + str(filename) + "\n" + traceback.format_exc())
    return list(error_data)
   

# filename is full path to file
# return data array
def read_file(filename):
  try:
    with open(filename) as f:
      contents = f.read()
    return parse_file_contents(contents, filename)
  except:
    util.err_log("mailfile: could not find " + str(filename))
    return list(error_data)

## DEPRECATED
#def read_file(filename):
#  data = []
#  try:
#    with open(filename) as f:
#      for i in range(len(fields)-1):
#        line = f.readline()
#        data.append(line[len(fields[i]) + 2 : -1])  # erase "field: " and trailing "\n"
#      f.readline()  # separator ==========
#      data.append(f.read()) # body
#  except:
#    util.err_log("mailfile: could not find " + str(filename) + "\n")
#    return list(error_data)
#  return data

# filename is a full path
def write_to_file(filename, data):
  os.makedirs(filename[:filename.rfind("/")], exist_ok=True)
  with open(filename, "w") as f:  # if not, crash because that's bad!!
    f.write(data_to_str(data))


#-------------------------------
# formatting of things

# turn refstr into list of msgids
# if my_id is not None, make sure to remove it from refs
# if it is None, ignore it
def do_get_referencelist(my_id, refstr):
  reflist = []
  if len(refstr) == 0:
    return reflist
  for r in refstr.split(", "):
    rs = r.strip()
    if len(rs) > 0 and (my_id is None or rs != my_id):
      reflist.append(rs)
  return reflist


def do_get_parent_id(my_id, reflist):
  if len(reflist) == 0 or reflist[-1] == my_id:
   return None
  return reflist[-1]


# given data array, return string to write to file
def data_to_str(data):
  a = []
  for i,field in enumerate(fields[:-1]):
    a.append(field)
    a.append(": ")
    a.append(data[i])
    a.append("\n")
  a.append(sep)
  a.append(data[BODY_L])
  return "".join(a)


# lazily loaded mailfiles
class MailMgr:
  def __init__(self):
    self.ids = msgids.Ids()
    # map filename to data array
    # None or not present if data is not loaded
    self.datas = {}
    
  def ensure_loaded(self, fname):
    if fname not in self.datas or self.datas[fname] is None:
      self.datas[fname] = read_file(fname)
        
  def _get(self, fname, ind):
    self.ensure_loaded(fname)
    return self.datas[fname][ind]

  def _set(self, fname, ind, val):
    self.ensure_loaded(fname)
    self.datas[fname][ind] = val.replace("\n","")
    write_to_file(fname, self.datas[fname])

  def get_all(self, fname):
    self.ensure_loaded(fname)
    return self.datas[fname]

  # ind should be FROM_L, TO_L, CC_L, BCC_L, REPLY_L
  # return pairlist (see bomail.util.addr)
  def get_addrlist(self, fname, ind):
    return util.str_to_pairlist(self._get(fname, ind))

  # ind should be FROM_L, TO_L, CC_L, BCC_L, REPLY_L
  # takes a pairlist (see bomail.util.addr)
  def set_addrlist(self, fname, ind, prlist):
    self._set(fname, ind, util.pairlist_to_str(prlist))

  def get_subj(self, fname):
    return self._get(fname, SUBJ_L)

  def set_subj(self, fname, newsub):
    self._set(fname, SUBJ_L, newsub)

  def get_dateobj(self, fname):
    datestr = self._get(fname, DATE_L)
    try:
      return dateutil.parser.parse(datestr)
    except:
      return datetime.datetime(year=9999,month=12,day=1)

  def is_sent(self, fname):
    return self._get(fname, SENT_L) == "True"

  def get_msgid(self, fname):
    self.ensure_loaded(fname)
    return self.datas[fname][MSG_ID_L]

  def get_references(self, fname):
    my_id = self.get_msgid(fname)
    refstr = self._get(fname, REFS_L)
    return do_get_referencelist(my_id, refstr)

  # get the id of the message that 'fname' replies to, or None
  def get_parent_id(self, fname):
    return do_get_parent_id(self.get_msgid(fname), self.get_references(fname))

  # get msgids that I am referenced by
  def get_refby(self, fname):
    s = self._get(fname, REF_BY_L)
    return do_get_referencelist(None, s)

  # update when we notice a new email referencing this one
  def add_refby(self, fname, refd_by):
    prev_refbys = set(self.get_refby(fname))
    prev_refbys.add(refd_by)
    self._set(fname, REF_BY_L, ", ".join(sorted(list(prev_refbys))))

  def set_refby(self, fname, refby_list):
    self._set(fname, REF_BY_L, ", ".join(sorted(refby_list)))

  def get_attachlist(self, fname):
    self.ensure_loaded(fname)
    s = self.datas[fname][ATTACH_L]
    return attach.attach_str_to_paths(s)

  def set_attachlist(self, fname, pathlist):
    self._set(fname, ATTACH_L, attach.attach_paths_to_str(pathlist))

  def get_state(self, fname):
    return self._get(fname, STATE_L)

  def set_state(self, fname, newstate):
    self._set(fname, STATE_L, newstate)

  def get_tags(self, fname, include_folders=False):
    return tags.get_taglist_from_str(self._get(fname, TAGS_L), include_folders=include_folders)

  def get_tagset(self, fname, include_folders=False):
    return tags.get_tagset_from_str(self._get(fname, TAGS_L), include_folders=include_folders)

  def set_tags(self, flist, taglist):
    tagstr = tags.get_str_from_taglist(taglist)  # clean tags
    for fname in flist:
      self._set(fname, TAGS_L, tagstr)

  def add_tags(self, flist, my_taglist):
    taglist = tags.get_taglist_from_list(my_taglist)  # clean, sort tags
    if len(taglist) > 0:
      for fname in flist:
        new_taglist = tags.join(taglist, self.get_tags(fname))
        self._set(fname, TAGS_L, tags.get_str_from_clean_taglist(new_taglist))

  def remove_tags(self, flist, my_taglist):
    taglist = tags.get_taglist_from_list(my_taglist)  # clean, sort tags
    if len(taglist) > 0:
      for fname in flist:
        new_taglist = tags.get_nonmatching(self.get_tags(fname), taglist)
        self._set(fname, TAGS_L, tags.get_str_from_clean_taglist(new_taglist))

  def get_body(self, fname):
    return self._get(fname, BODY_L)

  # if there was an update to these files (added, changed, or deleted),
  # just remove them from dict and re-load at next request
  def updated_flist(self, flist):
    for f in flist:
      if f in self.datas:
        del self.datas[f]

  def create(self, flist, datalist):
    idlist = [d[MSG_ID_L] for d in datalist]
    self.ids.add(idlist, flist)
    for fname,d in zip(flist, datalist):
      self.datas[fname] = d
      write_to_file(fname, d)
    # now that they're all written, update referenced-by info
    for fname in flist:
      for i_ref in self.get_references(fname):
        if self.ids.has(i_ref):
          self.add_refby(self.ids.get(i_ref), self.get_msgid(fname))

  # when files are deleted
  def update_for_trashed(self, flist, idlist):
    for fname in flist:
      if fname in self.datas:  # which it should be...
        del self.datas[fname]
    self.ids.remove(idlist, flist)


def main_cli():
  if len(sys.argv) <= 1 or "-h" in sys.argv:
    print(usage_str)
    exit(0)
  flist = [line.strip() for line in sys.stdin.readlines()]
  mgr = MailMgr()
  if sys.argv[1] == "-g":
    if len(sys.argv) <= 2:
      print(usage_str)
      exit(0)
    if sys.argv[2] == "all":
      print("\n\n".join([data_to_str(mgr.get_all(fname)) for fname in flist]))
    else:
      inds = [fields.index(s) for s in sys.argv[2:]]
      data = mgr.get_all(fname)
      print("\n\n".join(["\n".join([data[i] for i in inds]) for fname in flist]))
  elif sys.argv[1] == "-add-tags":
    taglist = sys.argv[2:]
    mgr.add_tags(flist, taglist)
  elif sys.argv[1] == "-rm-tags":
    taglist = sys.argv[2:]
    mgr.remove_tags(flist, taglist)
  else:
    print(usage_str)


if __name__ == "__main__":
  main_cli()

