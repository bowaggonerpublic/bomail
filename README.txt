bomail is an experimental system for organizing and interacting with email.
It can be installed with pip3 (the python package manager) via
    pip3 install bomail.
This software is free.


Homepage and information:
    https://www.bowaggoner.com/bomail/

Source:
    https://www.bitbucket.org/bowaggonerpublic/bomail

Version:
    0.9.4.2


TODOs:
 - Tree view for threads (see reply structure rather than list by date)
 - Digital signatures and encryption

Completed:
 - (0.9.3.0) Use XDG spec for where to put data and config
 - (0.9.2.0) Respect the reply-to header on emails

